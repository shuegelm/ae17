# README #

Wir leben in einer Welt, in der Daten gesammelt werden. Dabei reicht die Datenmenge von wenigen Zeichen, wie die 160 Zeichen einer vollausgenutzten Short Message des Short Message Service (kurz: SMS), bis hin zu großen Datenmengen von 14 Millionen Suchanfragen, welche täglich bei einem Suchmaschinenanbieter wie Duckduckgo eingehen. Diese Datenmengen können zur Informationsgewinnung analysiert werden. Ein anwendbares Analyseverfahren ist zum Beispiel die Wortschatzanalyse. Hierbei stellt die Redundanz in den Datensätzen jedoch ein Problem dar. Die hier vorliegende Arbeit beschäftigt sich mit dem Entfernen von Redundanzen, das sogenannte „Removing Duplicates“-Problem.

Hierzu wurden zwei Varianten eines Algorithmus in der Programmiersprache Java entwickelt, welche einen beliebigen Text einlesen, säubern, in einer unterschiedlichen Reihenfolge analysieren und im Anschluss die Redundanzen löschen, um folglich eine Wortschatzanalyse des Textes zurückzuliefern. Hierbei gibt es keine Einschränkungen auf die Länge eines Wortes oder seines Wortstammes. Beim Säubern werden über einen regulären Ausdruck nur Groß- und Kleinbuchstaben zugelassen. Dies erlaubt die Wortschatzanalyse von jeder lateinischen Sprache.


### What is this repository for? ###

* Quick summary
HTWK-Leipzig - Projekt Remove Duplicates für das Modul Algorithm Engineering im Wintersemester 2017/2018
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact