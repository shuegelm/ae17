package main;

import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Main {

    private static ArrayList<Pair<Integer,Pair<String,Integer>>> arrayListFromSequence = new ArrayList<Pair<Integer, Pair<String, Integer>>>();
    private static ArrayList<Pair<Integer,Pair<String,Integer>>> unsortedArrayList = new ArrayList<Pair<Integer, Pair<String, Integer>>>();
    private static ArrayList<Pair<Integer,Pair<String,Integer>>> sortedArrayList = new ArrayList<Pair<Integer,Pair<String,Integer>>>();
    private static final String PATH_FILENAME = "src/main/resources/10k.txt";

    public static void main(final String[] args) {
        System.out.println("I bims ein Java Programm");

        //System.out.println("Lade File ein");
        //loadFile(PATH_FILENAME);
        //System.out.println(arrayListFromSequence.size() + " Pairs eingelesen");

        //double durationArray10k[] = Start(40,"10k.txt");

        //Array von 0..2, 0 ist loadFile, 1 ist unsorted, 2 ist quicksort
        int[] speeches = {5000,10000,20000,30000,40000,50000};
        //int[] speeches = {2500,5000,10000,20000,40000,80000,160000};
        //int[] speeches = {1000,2000,3000,4000,5000,10000};
        //int[] speeches = {200};
        double[][][] tableWithResults = new double[speeches.length][4][18];
        //int[] speeches = {3};
        for (int i = 0; i < speeches.length;i++) {
            System.out.println("Problemgroesse: " + speeches[i] + " wurde gestartet!");

            //Testdaten
            //double[][] durationArray = Start(40,"100k.txt","testdaten",speeches[i],"LoadCompSort");
            //double[][] durationArray = Start(40,"100k.txt","testdaten",speeches[i],"LoadSortComp");

            //Realdaten
            //double[][] durationArray = Start(40,"speeches.txt","trump",speeches[i],"LoadCompSort");
            double[][] durationArray = Start(2,"speeches.txt","trump",speeches[i],"LoadSortComp");



            //Rechnerei für Hashcode
            tableWithResults[i][0] = Statistics.evaluateDurations(speeches[i],durationArray[0]);
            //Rechnerei für Compare
            tableWithResults[i][1] = Statistics.evaluateDurations(speeches[i],durationArray[1]);
            //Rechnerei Quicksort
            tableWithResults[i][2] = Statistics.evaluateDurations(speeches[i],durationArray[2]);
            //Rechnerei für alle zusammen
            tableWithResults[i][3] = Statistics.evaluateDurations(speeches[i],addAllDurations(durationArray));
            System.out.println("Problemgroesse: " + speeches[i] + " wurde beendet!");
        }


        //tableWithResults[0] = Statistics.evaluateDurations(10000,Start(40,"10k.txt"));
        //tableWithResults[0] = Statistics.evaluateDurations(10000,durationArray10k);



        WriteToFile.writeToFile(tableWithResults);
        WriteToFile.writeToFile(sortedArrayList);
    }

    private static double[] addAllDurations(double[][] durationArrayTrump) {
        double[] result = new double[durationArrayTrump[0].length];
        for (int i = 0; i < durationArrayTrump[0].length;i++) {
            result[i] = (durationArrayTrump[0][i]+ durationArrayTrump[1][i]+ durationArrayTrump[2][i]);
        }
        return result;
    }

    private static double[][] Start(int amountRun, String textFilePath, String codename, int problemSize, String algoType) {

        double[][] durations = new double[3][amountRun];
        cleanArrayLists();
        if (algoType.equals("LoadCompSort")) {
            for (int i = 1; i <= amountRun+10; i++) {
                double duration = 0, startTime1 = 0, startTime2 = 0, endTime = 0;

                startTime1 = System.nanoTime();
                startTime2 = System.nanoTime();

                arrayListFromSequence = loadFile(textFilePath, codename, problemSize);

                endTime = System.nanoTime();
                duration = endTime - 2*startTime2 + startTime1;

                if (i>10) {
                    //System.out.println("Durchgang: " + i + " schreibe ins Array");
                    durations[0][i-11] = (duration/1000000F);
                    //System.out.println(durations[i-11]);
                }

                startTime1 = System.nanoTime();
                startTime2 = System.nanoTime();

                unsortedArrayList = ComparisonOnDemand.compareOnDemand(arrayListFromSequence);

                endTime = System.nanoTime();
                duration = endTime - 2*startTime2 + startTime1;

                if (i>10) {
                    //System.out.println("Durchgang: " + i + " schreibe ins Array");
                    durations[1][i-11] = (duration/1000000F);
                    //System.out.println(durations[i-11]);
                }

                startTime1 = System.nanoTime();
                startTime2 = System.nanoTime();

                sortedArrayList = QuickSort.quickSort(unsortedArrayList,0,unsortedArrayList.size()-1);

                endTime = System.nanoTime();
                duration = endTime - 2*startTime2 + startTime1;

                if (i>10) {
                    //System.out.println("Durchgang: " + i + " schreibe ins Array");
                    durations[2][i-11] = (duration/1000000F);
                    //System.out.println(durations[i-11]);
                }
            }
        }
        if (algoType.equals("LoadSortComp")) {
            for (int i = 1; i <= amountRun+10; i++) {
                double duration = 0, startTime1 = 0, startTime2 = 0, endTime = 0;
                System.out.println("Durchlauf "+i);
                startTime1 = System.nanoTime();
                startTime2 = System.nanoTime();

                arrayListFromSequence = loadFile(textFilePath, codename, problemSize);

                endTime = System.nanoTime();
                duration = endTime - 2*startTime2 + startTime1;

                if (i>10) {
                    //System.out.println("Durchgang: " + i + " schreibe ins Array");
                    durations[0][i-11] = (duration/1000000F);
                    //System.out.println(durations[i-11]);
                }

                startTime1 = System.nanoTime();
                startTime2 = System.nanoTime();

                unsortedArrayList = QuickSort.quickSort(arrayListFromSequence,0,arrayListFromSequence.size()-1);
                //sortedArrayList = QuickSort.quickSort(new ArrayList<Pair<Integer,Pair<String,Integer>>>(arrayListFromSequence),0,arrayListFromSequence.size()-1);
                //System.out.println("Sort nach Einlesen");
                //System.out.println(unsortedArrayList);

                endTime = System.nanoTime();
                duration = endTime - 2*startTime2 + startTime1;

                if (i>10) {
                    //System.out.println("Durchgang: " + i + " schreibe ins Array");
                    durations[2][i-11] = (duration/1000000F);
                    //System.out.println(durations[i-11]);
                }

                startTime1 = System.nanoTime();
                startTime2 = System.nanoTime();

                sortedArrayList = ComparisonOnDemand.compareOnDemand(unsortedArrayList,"overload");
                //System.out.println("Compare nach sortiert");
                //System.out.println(sortedArrayList);

                endTime = System.nanoTime();
                duration = endTime - 2*startTime2 + startTime1;

                if (i>10) {
                    //System.out.println("Durchgang: " + i + " schreibe ins Array");
                    durations[1][i-11] = (duration/1000000F);
                    //System.out.println(durations[i-11]);
                }
            }

        }


        return durations;
    }

    private static void cleanArrayLists() {
        arrayListFromSequence.clear();
        unsortedArrayList.clear();
        sortedArrayList.clear();
    }

    private static ArrayList<Pair<Integer, Pair<String, Integer>>> loadFile(String fileName, String codename, int problemSize) {

        if (fileName!="") {
            //hashMapFromSequence = TextFileLoader.readFile(fileName);
            if (codename.equals("trump")) {
                return TextFileLoader.cleanReadFile("src/main/resources/"+fileName,problemSize);
            }
            else {
                return TextFileLoader.readFile("src/main/resources/"+fileName, problemSize);
            }
        }

        return null;
    }
}