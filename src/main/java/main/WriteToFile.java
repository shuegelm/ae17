package main;

import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class WriteToFile {
    public static void writeToFile(double[][][] durationArrayList) {

        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(new File("src/main/resources/hash.txt"));
            printWriter.println("n\tEX\tAnzahl\tsdv\tEXsdv\tAnzahl\tkleiner\tgrößer\tEX10%\tAnzahl\tEXboxplot\tQ025\tQ050\tQ075\ta\tb\tmin\tmax");
            for (double[][] problemzahlArray : durationArrayList) {
                for (int i = 0;i < problemzahlArray[0].length;i++) {
                    //System.out.println(problemzahlArray[0][i]);
                    printWriter.print(problemzahlArray[0][i]+"\t");
                }
                printWriter.print("\n");
            }
            printWriter.close();
            printWriter = new PrintWriter(new File("src/main/resources/compare.txt"));
            printWriter.println("n\tEX\tAnzahl\tsdv\tEXsdv\tAnzahl\tkleiner\tgrößer\tEX10%\tAnzahl\tEXboxplot\tQ025\tQ050\tQ075\ta\tb\tmin\tmax");
            for (double[][] problemzahlArray : durationArrayList) {
                for (int i = 0;i < problemzahlArray[1].length;i++) {
                    printWriter.print(problemzahlArray[1][i]+"\t");
                }
                printWriter.print("\n");
            }
            printWriter.close();
            printWriter = new PrintWriter(new File("src/main/resources/quicksort.txt"));
            printWriter.println("n\tEX\tAnzahl\tsdv\tEXsdv\tAnzahl\tkleiner\tgrößer\tEX10%\tAnzahl\tEXboxplot\tQ025\tQ050\tQ075\ta\tb\tmin\tmax");
            for (double[][] problemzahlArray : durationArrayList) {
                for (int i = 0;i < problemzahlArray[2].length;i++) {
                    printWriter.print(problemzahlArray[2][i]+"\t");
                }
                printWriter.print("\n");
            }
            printWriter.close();
            printWriter = new PrintWriter(new File("src/main/resources/full.txt"));
            printWriter.println("n\tEX\tAnzahl\tsdv\tEXsdv\tAnzahl\tkleiner\tgrößer\tEX10%\tAnzahl\tEXboxplot\tQ025\tQ050\tQ075\ta\tb\tmin\tmax");
            for (double[][] problemzahlArray : durationArrayList) {
                for (int i = 0;i < problemzahlArray[3].length;i++) {
                    printWriter.print(problemzahlArray[3][i]+"\t");
                }
                printWriter.print("\n");
            }
            printWriter.close();
            printWriter = new PrintWriter(new File("src/main/resources/_boxplot.txt"));
            printWriter.println("% N erw stdv q25 q75 median uwhi owhi boxwh");
            for (double[][] problemzahlArray : durationArrayList) {
                    printWriter.print(problemzahlArray[3][0]+" "+problemzahlArray[3][10]+" "+problemzahlArray[3][3]+" "+problemzahlArray[3][11]+" "+problemzahlArray[3][13]+" "+problemzahlArray[3][12]+" "+problemzahlArray[3][14]+" "+problemzahlArray[3][15]+" 0.2");
                printWriter.print("\n");
            }
            printWriter.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static void writeToFile(ArrayList<Pair<Integer, Pair<String, Integer>>> sortedArrayList) {
        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(new File("src/main/resources/text.txt"));
            printWriter.println("sequenceStringIntPair");
            for (Pair<Integer, Pair<String, Integer>> pairs : sortedArrayList) {
                printWriter.println(pairs.getValue().getKey() + " " + pairs.getValue().getValue());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        printWriter.close();
    }
}
