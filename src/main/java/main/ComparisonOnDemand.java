package main;

import com.sun.org.apache.xpath.internal.SourceTree;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;

public class ComparisonOnDemand {

    public static ArrayList<Pair<Integer,Pair<String,Integer>>> compareOnDemand(ArrayList<Pair<Integer,Pair<String,Integer>>> arrayList) {
        ArrayList<Pair<Integer,Pair<String,Integer>>> uniqueArrayList = new ArrayList<Pair<Integer,Pair<String,Integer>>>();
        int counter = 0;

        while (!arrayList.isEmpty()) {
            Pair<Integer,Pair<String,Integer>> chosenPair = arrayList.get(0);
            arrayList.remove(0);
            for (int j = 0; j < arrayList.size(); j++) {
                Pair<Integer,Pair<String,Integer>> otherPair = arrayList.get(j);
                counter++;
                //System.out.println(counter+" "+chosenPair.getKey() + " und " + otherPair.getKey());
                //String Vergleich nun als Hash
                //equals funzt, == nicht
                int a = chosenPair.getKey();
                int b = otherPair.getKey();
                //if (chosenPair.getKey()==otherPair.getKey()) {
                if (a == b) {
                    //Wenn Hashs gleich, dann schauen, welches kleinere Integer ist
                    //System.out.println(chosenPair.getValue().getValue() +" > "+ otherPair.getValue().getValue());
                    if ((chosenPair.getValue().getValue()) > (otherPair.getValue().getValue())) {
                        chosenPair = otherPair;
                    }
                    arrayList.remove(otherPair);
                    //beim löschen ist die arraylist 1 zu groß und muss somit eins früher wieder vergleichen
                    j--;
                }
                //System.out.println(arrayList);
            }
            uniqueArrayList.add(chosenPair);
        }

        //System.out.println(hashMap);
        System.out.println(counter + " Vergleiche vollzogen");

        return uniqueArrayList;
    }

    public static ArrayList<Pair<Integer,Pair<String,Integer>>> compareOnDemand(ArrayList<Pair<Integer,Pair<String,Integer>>> arrayList,String overload) {

        //System.out.println("Overload Funktion");
        ArrayList<Pair<Integer,Pair<String,Integer>>> uniqueArrayList = new ArrayList<Pair<Integer,Pair<String,Integer>>>();
        //HashMap<String,Integer> hashMap = new HashMap<String, Integer>();
        int counter = 1;

        while (!arrayList.isEmpty()) {
            Pair<Integer,Pair<String,Integer>> chosenPair = arrayList.get(0);
            arrayList.remove(0);
            for (int j = 0; j < arrayList.size(); j++) {
                Pair<Integer,Pair<String,Integer>> otherPair = arrayList.get(j);

                int a = chosenPair.getKey();
                int b = otherPair.getKey();
                //System.out.println("Vergleiche " + chosenPair + " mit " + otherPair + " und j ist " + j);
                counter++;
                if (a != b) {
                    break;
                } else {
                    //Wenn Hashs gleich, dann schauen, welches kleinere Integer ist
                    //System.out.println(chosenPair.getValue().getValue() +" > "+ otherPair.getValue().getValue());
                    if ((chosenPair.getValue().getValue()) > (otherPair.getValue().getValue())) {
                        chosenPair = otherPair;
                    }
                    arrayList.remove(otherPair);
                    //beim löschen ist die arraylist 1 zu groß und muss somit eins früher wieder vergleichen
                    j--;
                    //System.out.println(arrayList);
                }
                //if (a!=b) break;
            }
            uniqueArrayList.add(chosenPair);
        }

        //System.out.println(hashMap);
        System.out.println(counter + " Vergleiche vollzogen");

        return uniqueArrayList;
    }
}
