package main;

import javafx.util.Pair;

import java.util.ArrayList;

public class QuickSort {

    public static ArrayList<Pair<Integer,Pair<String,Integer>>> quickSort(ArrayList<Pair<Integer,Pair<String,Integer>>> strArr, int p, int r)
    {
        if(p<r)
        {
            int q=partition(strArr,p,r);
            quickSort(strArr,p,q);
            quickSort(strArr,q+1,r);
        }

        return strArr;
    }

    private static int partition(ArrayList<Pair<Integer,Pair<String,Integer>>> strArr, int p, int r) {

        String x = strArr.get(p).getValue().getKey();
        int i = p-1 ;
        int j = r+1 ;

        while (true)
        {
            i++;
            while ( i< r && strArr.get(i).getValue().getKey().compareTo(x) < 0)
                i++;
            j--;
            while (j>p && strArr.get(j).getValue().getKey().compareTo(x) > 0)
                j--;

            if (i < j)
                swap(strArr, i, j);
            else
                return j;
        }
    }

    private static void swap(ArrayList<Pair<Integer,Pair<String,Integer>>> strArr, int i, int j)
    {
        Pair<Integer,Pair<String,Integer>> temp = strArr.get(i);
        strArr.set(i,strArr.get(j));
        strArr.set(j,temp);
    }
}
