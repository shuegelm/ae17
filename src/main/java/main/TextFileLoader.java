package main;


import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextFileLoader {

    public static ArrayList<Pair<Integer,Pair<String,Integer>>> readFile(String fileName, int problemSize) {

        ArrayList<Pair<Integer,Pair<String,Integer>>> arrayListFromSequence = new ArrayList<Pair<Integer,Pair<String,Integer>>>();

        try {

            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);

            String line = br.readLine();
            String[] array;



            int counter = 0;
            while (line !=null) {
                if (counter > 0) {
                    array = line.split(" ");
                    arrayListFromSequence.add(new Pair(array[0].hashCode(),new Pair(array[0],Integer.parseInt(array[1]))));
                }
                counter++;
                if (counter == problemSize) {
                    break;
                }
                line = br.readLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return arrayListFromSequence;
    }

    public static ArrayList<Pair<Integer,Pair<String,Integer>>> cleanReadFile(String fileName, int problemSize) {
        ArrayList<Pair<Integer,Pair<String,Integer>>> arrayListFromTrump = new ArrayList<Pair<Integer,Pair<String,Integer>>>();

        try {

            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);

            String line = br.readLine();
            String[] array;


            int positionWort = 0;
            while (line !=null) {
                line = getOnlyStrings(line);
                array = line.split(" ");
                for (int i = 0; i < array.length;i++) {
                    if (!array[i].equals("")) {
                        arrayListFromTrump.add(new Pair(array[i].hashCode(),new Pair(array[i],++positionWort)));
                    }
                    if (arrayListFromTrump.size()==problemSize) {
                        System.out.println("Groesse von: " + problemSize + " erreicht!");
                        break;
                    }
                }
                line = br.readLine();
                if (arrayListFromTrump.size()==problemSize) {
                    break;
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayListFromTrump;
    }

    public static String getOnlyStrings(String s) {
        Pattern pattern = Pattern.compile("[^a-z A-Z ]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }
}
