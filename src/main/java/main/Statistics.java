package main;

import java.util.Arrays;
import java.util.Collections;

public class Statistics {

    public static double[] evaluateDurations(double problemSize, double[] durations) {

        //aufsteigend sortiert
        Arrays.sort(durations);

//        for (double duration : durations) {
//            System.out.println(duration);
//        }

        double results[] = new double[18];
        results[0] = problemSize;

        double anzahlDerWerte = durations.length;
        double erwartungswert = 0;
        for (double duration : durations) {
            erwartungswert += duration;
        }
        erwartungswert = (erwartungswert / anzahlDerWerte);
        results[1] = erwartungswert;
        results[2] = anzahlDerWerte;

        double varianz = 0;
        double sdv = 0;
        for (double duration : durations) {
            varianz += (Math.pow((erwartungswert - duration), 2));
        }

        varianz = (varianz / anzahlDerWerte);
        sdv = Math.sqrt(varianz);
        results[3] = sdv;
        //System.out.println("Der sdv ist: " +sdv);

        double exsdv = 0;
        double exsdvUnten = (erwartungswert - sdv);
        double exsdvOben = (erwartungswert + sdv);
        //System.out.println("EX + sdv: "+exsdvOben);
        //System.out.println("EX - sdv: "+exsdvUnten);
        double anzahlExSdv = 0;
        double anzahlExSdvGroesser = 0;
        double anzahlExSdvKleiner = 0;
        for (double duration : durations) {
            //System.out.println("Duration ist: "+duration);
            if ((duration >= exsdvUnten) && (duration <= exsdvOben)) {
                exsdv += duration;
                anzahlExSdv++;
                //System.out.println("Liegt im +- SDV");
            } else {
                //System.out.println("Liegt nicht im +- sdv");
                if (duration < exsdvUnten) {
                    anzahlExSdvKleiner++;
                } else {
                    anzahlExSdvGroesser++;
                }
            }
        }

        exsdv = (exsdv / anzahlExSdv);
        //System.out.println("Exsdv: " +exsdv);
        results[4] = exsdv;
        results[5] = anzahlExSdv;
        results[6] = anzahlExSdvKleiner;
        results[7] = anzahlExSdvGroesser;

        double erwartungswertProzent = 0;
        double anzahlErwartungsWertProzent = 0;
        int startWert = (int)(anzahlDerWerte*0.1);
        int endWert = (int)(anzahlDerWerte*0.9);
        //System.out.println("Startwert: " + startWert + " und Endwert "+ endWert);

        for (int i = 0; i < anzahlDerWerte;i++) {
            if (i >= startWert && i < endWert) {
                erwartungswertProzent += durations[i];
                anzahlErwartungsWertProzent++;
                //System.out.println("Bin drin bei: " + i + " und es sind: " + anzahlErwartungsWertProzent);
            }
        }
        erwartungswertProzent = (erwartungswertProzent / anzahlErwartungsWertProzent);
        results[8] = erwartungswertProzent;
        results[9] = anzahlErwartungsWertProzent;

        double exboxplot =0, qQuarter=0, qMedian=0, qPastQuarter=0,qA=0, qB=0, qMin=0, qMax=0,qAnzahlWerte = 0;

        qQuarter = durations[(int) ((anzahlDerWerte*0.25)-1)];
        results[11] = qQuarter;
        qMedian = durations[(int) ((anzahlDerWerte*0.5)-1)];
        results[12] = qMedian;
        qPastQuarter = durations[(int) ((anzahlDerWerte*0.75)-1)];
        results[13] = qPastQuarter;
        qA = (qQuarter-(1.5F*(qPastQuarter-qQuarter)));
        results[14] = qA;
        qB = (qPastQuarter+(1.5F*(qPastQuarter-qQuarter)));
        results[15] = qB;
        qMin = durations[0];
        results[16] = qMin;
        qMax = durations[(int) (anzahlDerWerte-1)];
        results[17] = qMax;


        for (double duration : durations) {
            if (duration >= qA && duration <= qB) {
                exboxplot += duration;
                qAnzahlWerte++;
            }
        }

        exboxplot = (exboxplot / qAnzahlWerte);

        results[10] = exboxplot;

        return results;
    }
}
